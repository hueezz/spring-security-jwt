package com.huzz.security.util.log;


import com.huzz.security.entity.SysLog;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 默认模块日志收集
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 13:50
 */
@Service
public class DefaultLogDetailsServiceImpl implements LogDetailsService {
    @Override
    public Object requestParams(String moduleName, Operate operateType, Object[] requestParams) {
        return null;
    }

    @Override
    public List<Map<String, Object>> setLogDetails(SysLog log, String moduleName, Operate operateType, Object custParams) {
        return new ArrayList<>();
    }
}

