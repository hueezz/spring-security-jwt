package com.huzz.security.util.log;

import com.huzz.security.entity.SysLog;

import java.util.List;
import java.util.Map;

/**
 * 模块日志不同部分处理
 * 使用: 以"对象名称+LogDetailsServiceImpl"的方式命名(例如: SysUserLogDetailsServiceImpl), 并添加@Service注解,
 *      通过实现该类重写setLogDetails方法收集各自模块特有部分日志
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 13:20
 */
public interface LogDetailsService {

    /**
     * 从请求参数中获取仅需要的请求参数
     * 需要通过操作类型做区分以指定哪个业务操作的日志逻辑
     * @param moduleName 模块名
     * @param operateType 操作类型
     * @param requestParams 请求参数
     * @return 仅需要的请求参数
     */
    public Object requestParams(String moduleName, Operate operateType, Object[] requestParams);

    /**
     * 收集该模块特有的日志
     * 需要通过操作类型做区分以指定哪个业务操作的日志逻辑
     * @param log
     * @param moduleName 模块名
     * @param operateType 操作类型
     * @param custParams 从requestParams返回的参数
     * @return
     */
    List<Map<String, Object>> setLogDetails(SysLog log, String moduleName, Operate operateType, Object custParams);

}
