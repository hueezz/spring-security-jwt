package com.huzz.security.util.log;

/**
 * 业务操作类型
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 11:25
 */
public enum Operate {

    SELECT("数据查询"),

    INSERT("数据新增"),

    SCHEDULE("任务调度"),

    UPDATE("数据修改"),

    DELETE("数据删除"),

    GRANT("系统授权"),

    EXPORT("数据导出"),

    IMPORT("数据导入"),

    GO_BACK("回退"),

    UPLOAD("文件上传"),

    DOWNLOAD("文件下载"),

    OTHER("其它"),

    RESET("重置"),

    SWITCH("切换状态"),

    EMPTY(""),

    LOGIN("登录"),

    NOTICE("通知"),

    LOGOUT("注销数据"),
    ;

    private String value;

    Operate(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
