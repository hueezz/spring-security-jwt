package com.huzz.security.util.log;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 11:02
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 业务名称
     */
    String name();

    /**
     * 操作类型
     * @return o
     */
    Operate operateType();

    /**
     * 是否保存日志
     * true: 保存, false: 不报错
     * @return boolean
     */
    boolean isSave() default true;

    /**
     * 保存请求参数
     * 当 isSave 是 false 时, 该属性设置不会生效
     */
    boolean saveReqParam() default true;

    /**
     * 保存响应结果
     * 当 isSave 是 false 时, 该属性设置不会生效
     */
    boolean saveResResult() default true;

}
