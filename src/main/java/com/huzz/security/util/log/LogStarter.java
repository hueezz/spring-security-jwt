package com.huzz.security.util.log;

import java.lang.annotation.*;

/**
 * 请填写业务名称
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 11:27
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogStarter {

    /**
     * 模块名称
     * @return string
     */
    String value() default "";

}
