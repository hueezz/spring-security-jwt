package com.huzz.security.util.captcha;

public class CaptchaType {

    public static final String MATH = "math";

    public static final String CHAR = "char";
}