package com.huzz.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huzz.security.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
* 系统日志-业务数据实体映射
*
* @author 童年的纸飞机
* @since 2022-10-09 17:12
*/
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}