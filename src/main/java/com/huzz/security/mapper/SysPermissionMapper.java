package com.huzz.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huzz.security.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;
/**
* 系统权限-业务数据实体映射
*
* @author 童年的纸飞机
* @since 2023-01-12 14:10
*/
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {


}