package com.huzz.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huzz.security.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
/**
* 系统用户角色-业务数据实体映射
*
* @author 童年的纸飞机
* @since 2023-01-12 14:13
*/
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {


}