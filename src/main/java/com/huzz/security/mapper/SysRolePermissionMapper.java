package com.huzz.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huzz.security.entity.SysRolePermission;
import org.apache.ibatis.annotations.Mapper;
/**
* 系统角色权限-业务数据实体映射
*
* @author 童年的纸飞机
* @since 2023-01-12 14:14
*/
@Mapper
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {


}