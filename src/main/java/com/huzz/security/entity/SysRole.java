package com.huzz.security.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
* 系统角色 实体类
*
* @author 童年的纸飞机
* @since 2023-01-12 14:08
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@TableName("sys_role")
@ExcelTarget("sysRole")
@ApiModel("系统角色 Entity Args")
public class SysRole implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    @Excel(name = "id")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    @Excel(name = "角色名称")
    @TableField(value = "username")
    private String name;

    @ApiModelProperty(value = "角色描述")
    @Excel(name = "角色描述")
    @TableField(value = "description")
    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    @Excel(name = "创建时间", format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "created_time")
    private Date createdTime;

    @ApiModelProperty(value = "创建人ID")
    @Excel(name = "创建人ID")
    @TableField(value = "created_user_id")
    private String createdUserId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "更新时间")
    @Excel(name = "更新时间", format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "更新人ID")
    @Excel(name = "更新人ID")
    @TableField(value = "update_user_id")
    private String updateUserId;

}