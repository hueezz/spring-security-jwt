package com.huzz.security.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

/**
 * 系统日志
 *
 * @author 童年的纸飞机
 * @create 2022-10-09 13:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@TableName("sys_log")
@ExcelTarget("sysLog")
@ApiModel("系统日志 Entity Args")
public class SysLog {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    @Excel(name = "id")
    private Integer id;

    @ApiModelProperty(value = "模块名称")
    @Excel(name = "模块名称")
    @TableField(value = "module_name")
    private String moduleName;

    @ApiModelProperty(value = "业务名称")
    @Excel(name = "业务名称")
    @TableField(value = "business_name")
    private String businessName;

    @ApiModelProperty(value = "操作类型")
    @Excel(name = "操作类型")
    @TableField(value = "operate_type")
    private String operateType;

    @ApiModelProperty(value = "操作人ID")
    @Excel(name = "操作人ID")
    @TableField(value = "operate_user_id")
    private String operateUserId;

    @ApiModelProperty(value = "请求IP")
    @Excel(name = "请求IP")
    @TableField(value = "request_ip")
    private String requestIp;

    @ApiModelProperty(value = "模块名称")
    @Excel(name = "模块名称")
    @TableField(value = "status")
    private Boolean status;

    @ApiModelProperty(value = "异常信息")
    @Excel(name = "异常信息")
    @TableField(value = "exp_message")
    private String expMessage;

    @ApiModelProperty(value = "请求参数")
    @Excel(name = "请求参数")
    @TableField(value = "request_params")
    private String requestParams;

    @ApiModelProperty(value = "返回结果")
    @Excel(name = "返回结果")
    @TableField(value = "response_params")
    private String responseParams;

    @ApiModelProperty(value = "日志详情")
    @Excel(name = "日志详情")
    @TableField(value = "log_details")
    private String logDetails;

    @ApiModelProperty(value = "创建时间")
    @Excel(name = "创建时间")
    @TableField(value = "created_time")
    private Date createdTime;

}
