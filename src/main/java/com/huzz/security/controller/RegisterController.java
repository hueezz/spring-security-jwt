package com.huzz.security.controller;

import cn.hutool.core.map.MapUtil;
import com.huzz.security.entity.SysUser;
import com.huzz.security.service.ISysUserService;
import com.huzz.security.util.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Name 名称
 * @Description 描述
 * @Author 童年的纸飞机
 * @Since 2023-01-2023/1/13 10:26
 */
@RestController
public class RegisterController {

    @Autowired
    private ISysUserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    public Result register(@RequestBody SysUser user) {
        if (StringUtils.isEmpty(user.getUsername())) {
            return Result.failed("用户名不能为空");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            return Result.failed("密码不能为空");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userService.save(user) ? Result.success("注册成功") : Result.failed("注册失败");
    }
}
