package com.huzz.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huzz.security.entity.SysUserRole;

/**
* 系统用户角色-业务模块接口
*
* @author 童年的纸飞机
* @since 2023-01-12 14:13
*/
public interface ISysUserRoleService extends IService<SysUserRole> {


}