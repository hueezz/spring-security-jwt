package com.huzz.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huzz.security.entity.SysUser;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
* 系统用户-业务模块接口
*
* @author 童年的纸飞机
* @since 2023-01-12 14:07
*/
public interface ISysUserService extends IService<SysUser> {

    SysUser getByUsername(String username);

    List<GrantedAuthority> getUserAuthorityInfo(Integer userId);
}