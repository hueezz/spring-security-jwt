package com.huzz.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huzz.security.entity.SysLog;

/**
* 系统日志-业务模块接口
*
* @author 童年的纸飞机
* @since 2022-10-09 17:12
*/
public interface ISysLogService extends IService<SysLog> {


}