package com.huzz.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huzz.security.entity.SysUserRole;
import com.huzz.security.mapper.SysUserRoleMapper;
import com.huzz.security.service.ISysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 系统用户角色-业务实现层
*
* @author 童年的纸飞机
* @since 2023-01-12 14:13
*/
@Service
@Slf4j
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    /**
     * 系统用户角色Mapper
     */
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

}