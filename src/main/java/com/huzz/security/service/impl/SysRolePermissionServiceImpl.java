package com.huzz.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huzz.security.entity.SysRolePermission;
import com.huzz.security.mapper.SysRolePermissionMapper;
import com.huzz.security.service.ISysRolePermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 系统角色权限-业务实现层
*
* @author 童年的纸飞机
* @since 2023-01-12 14:14
*/
@Service
@Slf4j
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {

    /**
     * 系统角色权限Mapper
     */
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

}