package com.huzz.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huzz.security.entity.SysLog;
import com.huzz.security.mapper.SysLogMapper;
import com.huzz.security.service.ISysLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 系统日志-业务实现层
*
* @author 童年的纸飞机
* @since 2022-10-09 17:12
*/
@Service
@Slf4j
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

}