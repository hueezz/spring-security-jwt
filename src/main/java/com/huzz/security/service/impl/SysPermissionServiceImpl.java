package com.huzz.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huzz.security.entity.SysPermission;
import com.huzz.security.mapper.SysPermissionMapper;
import com.huzz.security.service.ISysPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 系统权限-业务实现层
*
* @author 童年的纸飞机
* @since 2023-01-12 14:10
*/
@Service
@Slf4j
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    /**
     * 系统权限Mapper
     */
    @Autowired
    private SysPermissionMapper sysPermissionMapper;


}