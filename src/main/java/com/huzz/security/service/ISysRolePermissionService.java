package com.huzz.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huzz.security.entity.SysRolePermission;

/**
* 系统角色权限-业务模块接口
*
* @author 童年的纸飞机
* @since 2023-01-12 14:14
*/
public interface ISysRolePermissionService extends IService<SysRolePermission> {


}